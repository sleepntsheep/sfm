#define _(a...) {return({a;});}
#define C(a) case(a):
#define C2(x,a...) case(x):C(a)
#define C3(x,a...) case(x):C2(a)
#define C4(x,a...) case(x):C3(a)
#define C5(x,a...) case(x):C4(a)
#define R return
#define I(x, a...) if((x)){a;}
#define X(x, a...) if((x))_(a)
#define E(a...) else{a;}
#define W(x, a...) while((x)){a;}
#define i(s,e,a...) F(i,s,e,a)
#define F(i,s,e,a...) for(I i=(s),e_=(e);i<e;i++){a;}
#define Z sizeof
#define K break
#define O const
#define T typedef
#define M enum
#define S struct
#define ST static
#define G goto
#define SW(x,a...) switch(x){a;}
#define MX(a,b) ((a) > (b) ? (a) : (b))
#define MN(a,b) ((a) < (b) ? (a) : (b))

#define PM 4096


#include <curses.h>
#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <libgen.h>
#include <fcntl.h>
#include <fts.h>
#include <time.h>

T int I;T unsigned U;T void V;T char B;T size_t N;

ST O B*yesno[]={"yes","no"};

M{Dir,File};
T S En { I t; B b[PM]; S En **c; N a,l,s; } En;

En *ennew(int t, char *p)_(En*e=calloc(1,Z(En));e->t=t;strcpy(e->b,basename(p));
    I(t==File,S stat s;stat(p,&s);e->s=s.st_size;)e;)
V diradd(En*d,En*c){I(d->l==d->a,d->a=d->a*2+1;d->c=realloc(d->c,d->a*Z(En*))) d->c[d->l++]=c;}
V dirlist(En*d){
    DIR*r=opendir(".");S dirent*e=0;S stat s;d->l=0;En*c=0;
    W(e=readdir(r),stat(e->d_name,&s);c=ennew(S_ISDIR(s.st_mode)?Dir:File,e->d_name);diradd(d,c))
    closedir(r);}
V enfree(En*n){I(n->t==Dir,i(0,n->l,enfree(n->c[i]))free(n->c))free(n);}
I encmp(O V*a_,O V*b_)_(O En*a=*(En**)a_,*b=*(En**)b_;
    X(a->t==Dir&b->t==File,-1)X(a->t==File&b->t==Dir,1)strcmp(a->b, b->b);)
En*dirprep(En*d)_(dirlist(d);qsort(d->c,d->l,Z(En*),encmp);d;)
En*scd(En*o,B*p)_(En*n=ennew(Dir,p);chdir(p);dirprep(n);enfree(o);n;)
I srm(O B*p)_(remove(p);)
V stouch(O B*p){FILE*f=fopen(p,"w");fclose(f);}
B*scwd()_(ST B c[PM];getcwd(c,Z c);c;)
I smkpath(B *p)_(B*e=strchr(p,0),*v=p,prev[PM]=".",*v2;I(*p=='/',strcpy(prev,scwd());chdir("/"))
    W(1,v2=strchr(v,'/');I(!v2,K)*v2=0;mkdir(p,0777);*v2='/';v=v2+1;)
    I(e>p&&*(e-1)-'/')stouch(p); chdir(prev);0;)
V scp_file(O B*d,O B*s){I h;FILE*b=fopen(d,"wb"),*a=fopen(s,"rb");I(!b||!a,G f)
    W((h=fgetc(a))+1,fputc(h,b));f:I(a,fclose(a))I(b,fclose(b))}
V scp_recursive(B*da,B*sa) {B ds[PM],*sav[]={sa,0};N r=strrchr(sa,'/')-sa+1;
    FTS*f=fts_open((B*O*)sav,FTS_NOCHDIR|FTS_PHYSICAL,0);FTSENT*e=0;
    W(e=fts_read(f),I o=e->fts_info;I(o==FTS_NS||o==FTS_NSOK||o==FTS_F,sprintf(ds,"%s/%s",da,e->fts_path+r);
                smkpath(ds);scp_file(ds,e->fts_path)))fts_close(f);}
I srm_recursive(O B*p)_(O B*av[]={p,0};FTS*f=fts_open((B*O*)av,FTS_NOCHDIR|FTS_PHYSICAL,0);FTSENT*e;
    I(!f,R 1)W(e=fts_read(f),SW(e->fts_info,C5(FTS_DP,FTS_F,FTS_SL,FTS_SLNONE,FTS_DEFAULT)I(remove(e->fts_accpath)<0,R -1)K))fts_close(f);0)

WINDOW*gw,*gdw;I gd,dt,w,h,cur,ofs,quit,drwh; En*cwd;B dp[512],cwdp[PM],cpp[PM*2];

V cd(B*p){cwd=scd(cwd,p);strcpy(cwdp,scwd());}
V dpy(O B*f,...){va_list v;va_start(v,f);vsprintf(dp,f,v);va_end(v);}
V resize(){getmaxyx(stdscr,h,w);wresize(gw,h,w);wresize(gdw,h,w/2);mvwin(gdw,0,w/2);drwh=h-4;}
I dialog(O B*p,N no,O B**o,I s){I ml=strlen(p),c=s,ch,dh,dw,sy,sx;
    i(0,no,ml=MX(ml,strlen(o[i]+3)))dh=no+3;dw=ml+2;sy=h/2-dh/2;sx=w/2-dw/2;WINDOW*d=newwin(dh,dw,sy,sx);
        W(1,werase(d);box(d,0,0);mvwprintw(d,1,1,"%s",p);i(0,no,I(i==c,wattron(d,A_REVERSE))mvwprintw(d,2+i,1," > %s",o[i]);wattroff(d,A_REVERSE))wrefresh(d);
        ch=wgetch(d);SW(ch,C(KEY_RESIZE)resize();sy=h/2-dh/2;sx=w/2-dw/2;mvwin(d,sy,sx);K;
            C('q')R -1;C2('j',KEY_DOWN)c=MN(c+1,no-1);K;C2('k',KEY_UP)c=MX(s-1,0);K;C2(KEY_ENTER,'\n')R c;))}
V init(){initscr();cbreak();noecho();raw();keypad(stdscr,1);cwd=dirprep(ennew(Dir,scwd()));
    gw=newwin(h,w,0,0);keypad(gw,1);gdw=newwin(h,w/2,0,w/2);cd(".");dt=0;resize();}
V deinit() { delwin(gdw); delwin(gw); endwin(); }
En*selected()_(cwd->c[cur];)
V input() {I ch=wgetch(gw);B p[PM];En*e=selected();SW(ch,C(KEY_RESIZE)resize();K;C('c')sprintf(cpp,"%s/%s",cwdp,e->b);dpy("Copyed %s to clipboard", cpp);K; C('p')I(*cpp,scp_recursive(cwdp,cpp);dpy("Pasted %s to %s",cpp,cwdp);dirprep(cwd);*cpp=0;)K;
    C('i')dt=!dt;K;
    C2('k',KEY_UP)cur=MX(0,cur-1);I(cur<ofs,ofs--);K;
    C2('j',KEY_DOWN)cur=MN(cur+1,cwd->l-1);I(cur-ofs>=drwh,ofs++);K;
    C('q')quit=1;K;
    C('d')strcpy(p,e->b);I(srm(e->b),I(errno==ENOTEMPTY,I(dialog("Not empty, Delete?",2,yesno,1)==0,I(srm_recursive(e->b),dpy("Failed deleting: %s", strerror(errno)))E(dpy("Force-deleted %s",p)))))E(dpy("Deleted %s",p));dirprep(cwd);K;
    C2(KEY_LEFT,'h')cd("..");K;
    C('a')*p=0;mvwprintw(gw,h-3,1,"New file: ");echo();mvwgetnstr(gw,h-3,10,p,Z p);noecho();I(*p,smkpath(p);dirprep(cwd))K;
    C('r')mvwprintw(gw,h-3,1,"New name: ");echo();mvwgetnstr(gw,h-3,10,p,Z p);noecho();rename(e->b,p);dirprep(cwd);K;
    C4(KEY_RIGHT,KEY_ENTER,'\n','l')cd(e->b);ofs=cur=0;K;)
}
V drawdetail(){En*e=selected();I y=1,x=1;S stat s;stat(e->b, &s);werase(gdw); box(gdw,0,0);B m[256];
#define D1(fmt, a) mvwprintw(gdw, y++, x, fmt, a);
#define D2(fmt, a, b) mvwprintw(gdw, y++, x, fmt, a, b);
#define D3(fmt, a, b, c) mvwprintw(gdw, y++, x, fmt, a, b, c);
    D1("Name: %s", e->b);D1("Type: %s", e->t == Dir ? "Directory" : "File");
    I(e->t==File,S tm*tm=localtime(&s.st_mtime);strftime(m,Z m, "%d-%m-%y %H:%M", tm);D1("Last Modified: %s", m))
#undef D1
#undef D2
#undef D3
    wrefresh(gdw);
}

V draw(){I l=ofs,h=MN(ofs+drwh,cwd->l);clear();refresh();werase(gw);box(gw,0,0);
i(l,h,En*e=cwd->c[i];I(i==cur,wattron(gw,A_REVERSE))I(e->t==Dir,wattron(gw,A_BOLD))
        mvwprintw(gw,i-l+1,1," %s",e->b);wattroff(gw,A_BOLD);wattroff(gw,A_REVERSE);)
    mvwprintw(gw,h-1,1," %s ",dp);mvwprintw(gw,0,1," %s ",scwd());wrefresh(gw);
    I(dt,drawdetail()); }

I main()_(init();W(!quit,draw(),input())deinit();0)

